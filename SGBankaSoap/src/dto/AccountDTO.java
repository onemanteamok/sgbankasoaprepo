package dto;

import java.util.Date;

public class AccountDTO {

	private int id;
	private String account;
	private String cardNumber;
	private String cardType;
	private Date expirationDate;
	private String cardPIN;
	private int amount;
	private boolean active;
	private String jmbg;
	
	
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}
	 
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the cardPIN
	 */
	public String getCardPIN() {
		return cardPIN;
	}

	/**
	 * @param cardPIN the cardPIN to set
	 */
	public void setCardPIN(String cardPIN) {
		this.cardPIN = cardPIN;
	}

	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	/**
	 * @return the jmbg
	 */
	public String getJmbg() {
		return jmbg;
	}

	/**
	 * @param jmbg the jmbg to set
	 */
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccountDTO [id=" + id + ", account=" + account + ", cardNumber=" + cardNumber + ", cardType=" + cardType
				+ ", expirationDate=" + expirationDate + ", cardPIN=" + cardPIN + ", amount=" + amount + ", active="
				+ active + ", jmbg=" + jmbg + "]";
	}
	
	
	
	
}
