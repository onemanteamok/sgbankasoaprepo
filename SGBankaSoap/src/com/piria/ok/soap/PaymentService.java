package com.piria.ok.soap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import dao.AccountDAO;
import dto.AccountDTO;

public class PaymentService {

	// Predefined superuser registration fee.
	int registrationFee = 30;

	/**
	 * Pay registration fee for superuser.
	 * 
	 * @param cardNumber
	 * @return 1 - successful, 2 - something's wrong, 3 - exception emerged, 4 - account does not exist, 5 - not enough money, 6 - crediting account unsuccessful
	 */
	public int payForRegistration(String cardNumber, String cardType, String cardExpirationMonth,
			String cardExpirationYear, String cardPIN) {
		System.out.println("PaymentService payForRegistration parameters: " + cardNumber + " " + cardExpirationMonth+ " " +cardExpirationYear+ " " +cardPIN);

		try {
			Date expirationDate = new SimpleDateFormat("yyMM").parse(cardExpirationYear + cardExpirationMonth);
			System.out.println("paymentService expiration date: " + expirationDate);
			AccountDTO accountDTO = null;
			accountDTO = AccountDAO.getBy4Parameters(cardNumber, cardType, expirationDate, cardPIN);
			System.out.println("PaymentService payForRegistration accountDTO: " + accountDTO);

			if (accountDTO != null) {
				if (accountDTO.getAmount() >= registrationFee) {
					if (AccountDAO.subtractAmount(accountDTO.getId(), registrationFee) > 0)
						return 1;
					else
						return 6;
				} else
					return 5;
			} else
				return 4;

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 3;
		}
	}
	
}
