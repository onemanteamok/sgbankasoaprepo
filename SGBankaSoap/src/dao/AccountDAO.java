package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.AccountDTO;

public class AccountDAO {

	private static final String SQL_GET_BY_4_PARAMETERS = "SELECT * FROM accounts WHERE card_number=? AND card_type=? AND expiration_date>=? AND pin=?";
	private static final String SQL_SUBTRACT_AMOUNT = "UPDATE accounts SET amount=amount-? WHERE id=?";
	private static final String SQL_ACTIVATE = "UPDATE accounts SET active=1 WHERE id=?";

	
	public static AccountDTO getBy4Parameters(String cardNumber, String cardType, java.util.Date expirationDate, String cardPIN) {
		AccountDTO retVal = null;
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ppst = null;
		System.out.println("AccoutnDAO getBy4Pararmeters");
		try {
			conn = ConnectionPool.getConnectionPool().checkOut();
			System.out.println("AccoutnDAO getBy4Pararmeters after connectionPool checkout");
			ppst = conn.prepareStatement(SQL_GET_BY_4_PARAMETERS);
			ppst.setString(1, cardNumber);
			ppst.setString(2, cardType);
			ppst.setDate(3, new Date(expirationDate.getTime()));
			ppst.setString(4, cardPIN);
			System.out.println("AccoutnDAO getBy4Pararmeters berofe executeQuery");

			rs = ppst.executeQuery();
			System.out.println("AccoutnDAO getBy4Pararmeters after executeQuery");

			if (rs.next()) {
				retVal = new AccountDTO();
				retVal.setId(rs.getInt("id"));
				retVal.setAccount(rs.getString("account"));
				retVal.setCardNumber(rs.getString("card_number"));
				retVal.setCardType(rs.getString("card_type"));
				retVal.setExpirationDate(rs.getDate("expiration_date"));
				retVal.setCardPIN(rs.getString("pin"));
				retVal.setAmount(rs.getInt("amount"));
				retVal.setActive(rs.getBoolean("active"));
				retVal.setJmbg(rs.getString("jmbg"));
				System.out.println("AccoutnDAO accountDTO: " + retVal);
				return retVal;
			} else
				return null;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			if (ppst != null)
				try {
					ppst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			ConnectionPool.getConnectionPool().checkIn(conn);
		}	
	}
	
	public static int subtractAmount(int accountId, int amount) {
		Connection conn = null;
		PreparedStatement ppst = null;
		System.out.println("accountId: " + accountId + " and amount: " + amount);

		try {
			conn = ConnectionPool.getConnectionPool().checkOut();
			ppst = conn.prepareStatement(SQL_SUBTRACT_AMOUNT);
			ppst.setInt(1, amount);
			ppst.setInt(2, accountId);

			int result =  ppst.executeUpdate();
			System.out.println("result: " + result);
			return result;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		} finally {
			if (ppst != null)
				try {
					ppst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			ConnectionPool.getConnectionPool().checkIn(conn);
		}
	}
	
	public static int activate(int accountId) {
		Connection conn = null;
		PreparedStatement ppst = null;

		try {
			conn = ConnectionPool.getConnectionPool().checkOut();
			ppst = conn.prepareStatement(SQL_ACTIVATE);
			ppst.setInt(1, accountId);

			return ppst.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		} finally {
			if (ppst != null)
				try {
					ppst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			ConnectionPool.getConnectionPool().checkIn(conn);
		}

	}

	
//	public static int insert(AccountDTO accountDTO) {
//		if (userRegister.getPrivilege() != 2)
//			userRegister.setPrivilege(3);
//		userRegister.setActive(false);
//		Connection conn = null;
//		ResultSet rs = null;
//		PreparedStatement ppst = null;
//		try {
//			conn = ConnectionPool.getConnectionPool().checkOut();
//			ppst = conn.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
//			ppst.setString(1, userRegister.getFirstName());
//			ppst.setString(2, userRegister.getLastName());
//			ppst.setString(3, userRegister.getSocialNo());
//			ppst.setTimestamp(4, userRegister.getBirthDate());
//			ppst.setString(5, userRegister.getEmail());
//			ppst.setInt(6, userRegister.getPrivilege());
//			ppst.setString(7, userRegister.getUsername());
//			ppst.setString(8, userRegister.getPassword());
//			ppst.setBoolean(9, userRegister.isActive());
//			ppst.executeUpdate();
//			rs = ppst.getGeneratedKeys();
//			if (rs.next())
//				return rs.getInt(1);
//			else
//				return -1;
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return -1;
//		} finally {
//			if (ppst != null)
//				try {
//					ppst.close();
//				} catch (SQLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			ConnectionPool.getConnectionPool().checkIn(conn);
//		}
//		
//		return 1;
//	}
	
}
